# 職務経歴書 / resume

## わたしについて / About Me

以下のデータにご連絡をください。  
メールアドレスは、SPAM対策のため「`@`」を「`at_mark`」に変更しております。  
ご連絡の際には変更した上でご連絡ください。

| key | japanese | english |
|:--|:--|:--|
| 氏名 / Name | 大音 慧明 | Satoaki Ooto |
| Github | [satoaki-ooto](https://github.com/satoaki-ooto) |
| Gitlab | [satoaki-ooto](https://gitlab.com/satoaki-ooto) |
| リンクトイン / LinkedIn | [Satoaki Ooto](https://www.linkedin.com/in/satoaki-ooto-791a31144/) |
| 個人事業アドレス / Individual Mail Address | satoaki.ooto at_mark g.whiteball.me |
| 個人アドレス / Personal Mail Address | satoaki.ooto at_mark gmail.com |

## SNS関係

| key | japanese | english |
|:--|:--|:--|
| Twitter |[ooto_me](https://twitter.com/ooto_me)|
| Bluesky | [ooto.me](https://bsky.app/profile/ooto.me) |
| Mastodon | [@satoaki@sns.ooto.me](https://sns.ooto.me/@satoaki) |
| firefish | [@satoaki@firefish.ooto.me](https://firefish.ooto.me/@satoaki) |

## 職務経歴 / My Career

### 現職 / Current Company

#### [フリー株式会社](https://corp.freee.co.jp/) | 202104 - 

##### プロダクトセキュリティチームへの移動 | 202401 -

* 主な業務
  * 基本の守備範囲は社内セキュリティを中心に、エンジニアとして技術に関することをプロダクトセキュリティチームに集約される運びに
  * 現状は今までできていなかった暗黙知の内容を形式知に変え、より自分のタスクをチームにて執り行うことを行っています

##### 情報システム部門からの業務移管にともなう運用業務 | 202310 - 202401

* 主な業務
  * 情報システム部門から一部業務が移管されるかたちとなり、運用を行う形へ
  * 主にVPN Gatewayのインフラ運用、MDM関係を中心に運用を行いました

##### CSIRT（SOC業務）のチーム作り | 202204 - 

* 主な業務
  * もともとはビジネスパートナー様から来ていただいている方と二人で技術的な下支えをしてきました
  * この頃からチームに少しずつ大きくなり始め技術的な部分をベースとして、チームを作っています

##### EDR刷新プロジェクト | 202110 - 202203

* 主な業務
  * 当時利用していたEDRが様々な要因で刷新する必要になったため刷新を実施しました
  * 詳細は以下をご覧ください
    * [導入事例 - テクマトリックス](https://www.techmatrix.co.jp/casestudy/freee2/index.html)
    * [導入事例 - Palo Alto](https://www.paloaltonetworks.jp/apps/pan/public/downloadResource?pagePath=/content/pan/ja_JP/customers/freee)

##### CSIRT（SOC業務）のキャッチアップ | 202107 - 202109

* 主な業務
  * freeeに入った理由がCSIRTの中でも技術的な対応が求められる場面で回答や調整をすることが難しく埋めるためにオファーをいただきました
  * freeeのCSIRTはSOCも兼任していたのですが、技術的に対応ができる人間が当時CSIRT内におらずPSIRTメンバーが兼任をしていました
  * このためPSIRTメンバーが兼任していたことを３ヶ月ほどで引き継ぎを実施し、アラート対応や社内ネットワークの構成などをキャッチアップしていました

##### キャッチアップ時期 | 202104 - 202106

* 主な業務
  * freeeにjoinしてまず実施したのは労務周りのキャッチアップでした
  * アカウントの管理などがうまく行っておらず、入社から退社までの流れを把握しました
  * セキュリティ部門としてもこのあたりの把握をしておらず、システム面から把握する形を取りました

### 前職 / Previous Company

#### [バリオセキュア株式会社](https://www.variosecure.net/) | 201704 - 202103

##### Managed Service Provideを提供する上でのInfra構築活動　| 202004 - 202103

* 主な業務
  * 構築を実施した後、サービス提供を行う際に工数がかかっていたため、過去の経験を活かすためにインフラ構築の自動化を実施しました
  * サーバイメージは[Packer](https://www.packer.io/)を利用しておりました
  * OSの設定周りは [Chef](https://github.com/chef) を利用しておりましたが、退職に伴い [Ansible](https://github.com/ansible/ansible) へと移行する形になりました
  * CI/CDは [Gitlab](https://about.gitlab.com/ja-jp/) を用いてpackerやchef/ansibleの冪統さを担保する仕組みを作り運用をしていました

##### 上場にともなうCSIRT構築 | 201904 - 202003

* 主な業務
  * 導入支援や試験が全般的に終わったのと同時に、自社ないのセキュリティ向上活動を行うためにCSIRTと発足させ、上場前に古いOSやライブラリが利用されていたサーバの更新などを実施しました
  * また日本に未上陸であったSIEM製品であった [Humio](https://www.crowdstrike.jp/press-releases/crowdstrike-to-acquire-humio/) の導入を行い、自社製品のログを検索する環境構築などを行いました
  * これにより各種別々のログとなっていた自社製品のログを一気通貫で検索できるような形となって「誰がいつなにをしたのか？」ということをネットワークレベルで確認できるようになりました

##### 特定顧客に対する品質保証活動 | 201810 - 201903

* 主な業務
  * 旧官公庁に自社製品を導入するにあたり、納品前のネットワーク切り替え試験や上位ベンダーとの折衝を実施しました
  * 実際に運用チームが入れ込んだ設定を、導入時の仕様をベースに、試験項目を洗い出しを実施しました
  * テストスケジュールの遅延が発生した際には、現場での試験項目の実施の調整を行っていました

##### Managed Service Provideを提供する上での品質保証活動 | 201704 - 201809

* 主な業務
  * 品質保証を行う上「テストの自動化」ではなく「テスト環境を構築するための自動化」を行っていました
  * Service ProvideをしていたものがLinux Baseのものであったので、複数のIaCなどを技術検討を実施しました
  * 検証をした結果 [itamae](https://github.com/itamae-kitchen/itamae) を利用を考えましたが、最終的に導入まで至りませんでした。

## スキル / Skill

### 強み

* 特化したセキュリティ能力
  * ネットワークをベースとして防御面をベースに対応できる仕組みを考えることができる

### 伸ばしたいこと

* 実際のサイバーアタックをできるようにする

### 興味があること

* **侵入の痕跡を残させて、攻撃やインシデントそのものを再現できるような仕組みを作る**
* **Security Operation Teamを立ち上げて、世界の脅威に立ち向かえる仕組みを作る**
* **セキュリティチームがいなくなっても従業員たちが働いているだけで、脅威に対応できる仕組みを作る**

### インフラ / Infrastructure

#### Linux

* Fedora / RHEL / Cent OS Stream / Rocky Linux
* Ubuntu / Debian
* openSUSE
* Arch Linux
* Gentoo Linux

とりあえず一通り触っています。  
LAMP環境構築なども可能です。

### ネットワーク / Network

#### 対応ベンダー

##### 実務経験のあるもの
* Linux上のNetwork構築

##### 実務経験はないが経験で触ることができるもの
* Cisco

### セキュリティ / Security

#### ネットワーク / Network Security

##### 実務経験のあるもの
* Fortigate
* Palo Alto

##### 実務経験はないが経験で触ることができるもの
* Cicso Meraki

#### エンドポイント / Endpoint Security

##### 実務経験のあるもの
* Palo Alto Cortex
* SentinelOne

##### 実務経験はないが経験で触ることができるもの
* CrowdStrike

#### MDM

##### 実務経験はないが経験で触ることができるもの
* Intune
* Jamf

### プログラミングスキル / Programming

##### 実務経験のあるもの

* shell script
* python

##### 実務経験はないが経験で触ることができるもの

* ruby
* elixir
* crystal

### その他 / Other

* Google Workspaceの管理・運用
* Microsoft 365の管理・運用
